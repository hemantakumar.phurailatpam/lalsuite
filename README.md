# LALSuite

## for development of eccentric waveform with periastron advancement

# waveform list 

### PerAdvLeO (3PN, no spin, eccentric, 3 harmonics, periastron advance)

### YunesEccentric3harmonics (3PN, no spin, eccentric, 3 harmonics, no periastron advance)

### PerAdvFDCir (3PN, no spin, not eccentric, 1 harmonics, no periastron advance)
