f=30.00
phasing(f) = 6.667364e+01+i*0.000000e+00, phasing[(f_ref) = 5.561244e+02+i*0.000000e+00
f*shft=-18.849556, ll*phic=1.200000, psi=phasing(f)+f*shft-ll*phic-phasing(f_ref)=-5.095003e+02+i*0.000000e+00
xip=9.405405e-02+i*0.000000e+00, xic=0.000000e+00+i*9.400387e-02
f=30.00
phasing(f) = 4.271131e+02+i*0.000000e+00, phasing[(f_ref) = 3.124131e+03+i*0.000000e+00
f*shft=-18.849556, ll*phic=2.400000, psi=phasing(f)+f*shft-ll*phic-phasing(f_ref)=-2.718268e+03+i*0.000000e+00
xip=-3.996802e+00+i*0.000000e+00, xic=-0.000000e+00+i*-3.996800e+00
f=30.00
phasing(f) = 1.295124e+03+i*0.000000e+00, phasing[(f_ref) = 9.050209e+03+i*0.000000e+00
f*shft=-18.849556, ll*phic=3.600000, psi=phasing(f)+f*shft-ll*phic-phasing(f_ref)=-7.777534e+03+i*0.000000e+00
xip=-2.820117e-01+i*0.000000e+00, xic=-0.000000e+00+i*-2.820116e-01
Amp = 7.458120e-24, h+ = -2.707012e-24+i*3.095754e-23, hx = -3.095731e-23+i*-2.707063e-24

f=20.00
phasing(f) = 1.610712e+02+i*0.000000e+00, phasing[(f_ref) = 5.561244e+02+i*0.000000e+00
f*shft=-12.566371, ll*phic=1.200000, psi=phasing(f)+f*shft-ll*phic-phasing(f_ref)=-4.088196e+02+i*0.000000e+00
xip=1.442951e-01+i*0.000000e+00, xic=0.000000e+00+i*1.442181e-01
f=20.00
phasing(f) = 9.280768e+02+i*0.000000e+00, phasing[(f_ref) = 3.124131e+03+i*0.000000e+00
f*shft=-12.566371, ll*phic=2.400000, psi=phasing(f)+f*shft-ll*phic-phasing(f_ref)=-2.211021e+03+i*0.000000e+00
xip=-3.996802e+00+i*0.000000e+00, xic=-0.000000e+00+i*-3.996800e+00
f=20.00
phasing(f) = 2.727737e+03+i*0.000000e+00, phasing[(f_ref) = 9.050209e+03+i*0.000000e+00
f*shft=-12.566371, ll*phic=3.600000, psi=phasing(f)+f*shft-ll*phic-phasing(f_ref)=-6.338639e+03+i*0.000000e+00
xip=-4.326545e-01+i*0.000000e+00, xic=-0.000000e+00+i*-4.326544e-01
Amp = 1.196931e-23, h+ = -5.353119e-23+i*-2.890882e-24, hx = 2.891421e-24+i*-5.353138e-23

f=10.00
phasing(f) = 5.561244e+02+i*0.000000e+00, phasing[(f_ref) = 5.561244e+02+i*0.000000e+00
f*shft=-6.283185, ll*phic=1.200000, psi=phasing(f)+f*shft-ll*phic-phasing(f_ref)=-7.483185e+00+i*0.000000e+00
xip=2.999200e-01+i*0.000000e+00, xic=0.000000e+00+i*2.997600e-01
f=10.00
phasing(f) = 3.124131e+03+i*0.000000e+00, phasing[(f_ref) = 3.124131e+03+i*0.000000e+00
f*shft=-6.283185, ll*phic=2.400000, psi=phasing(f)+f*shft-ll*phic-phasing(f_ref)=-8.683185e+00+i*0.000000e+00
xip=-3.996802e+00+i*0.000000e+00, xic=-0.000000e+00+i*-3.996800e+00
f=10.00
phasing(f) = 9.050209e+03+i*0.000000e+00, phasing[(f_ref) = 9.050209e+03+i*0.000000e+00
f*shft=-6.283185, ll*phic=3.600000, psi=phasing(f)+f*shft-ll*phic-phasing(f_ref)=-9.883185e+00+i*0.000000e+00
xip=-8.992804e-01+i*0.000000e+00, xic=-0.000000e+00+i*-8.992801e-01
Amp = 2.687020e-23, h+ = 1.154169e-22+i*3.933549e-23, hx = -3.933300e-23+i*1.154180e-22

