/*
 *  Copyright (C) 2022 Phurailatpam Hemantakumar
 *  Assembled from code found in:
 *    - LALSimInspiralTaylorF2Ecc.c
 *    - LALSimInspiralEccentricityFD.c
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA  02110-1301  USA
 */

// #include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <lal/Date.h>
#include <lal/FrequencySeries.h>
#include <lal/LALConstants.h>
#include <lal/LALDatatypes.h>
#include <lal/LALSimInspiral.h>
#include <lal/Units.h>
#include <lal/XLALError.h>

////////////////////////////////////////////////////////////////////
// These functions are defined in LALSimInspiralPNCoeffPeriastronEccFD.h
////////////////////////////////////////////////////////////////////
// function to calculate PN coefficients for k (periastron advancement), et (eccentricity at f) and psi (fourier phase).
// void k_et_psi_PNe0Coeff1( REAL8*, REAL8*, REAL8*, chi_struct*, REAL8, REAL8 ); /* Harmonic indices independent */
// void et_psi_PNe0Coeff2( REAL8*, REAL8*, chi_struct*, REAL8, REAL8, REAL8, REAL8, REAL8 ); /* Harmonic indices dependent */
// for ref phasing clasulation
// void k_psi_PNe0Coeff1( REAL8*, REAL8*, chi_struct*, REAL8, REAL8 ); /* Harmonic indices independent */
// void psi_PNe0Coeff2( REAL8*, chi_struct*, REAL8, REAL8, REAL8, REAL8, REAL8 ); /* Harmonic indices dependent */
// void Xi_PlusCross( CS_*, eccn*, xi_* ); /*function for calculating xi (harmonics dependent amplitude) */
// function to calculate the harmonics for the given j and n values
// void harmonics( int, CS_*, sincos_*, eccn* );

#include "LALSimInspiralPNCoeffPerAdvFDLeO.h"

#ifndef _OPENMP3
#define omp3 ignore
#endif

// defining some useful global constant. But I have replace all these constants in the code
// static const REAL8 C =  299792458.0, G = 6.67408e-11, Gamma = 0.5772156649, C_3 = 2.694400241737399e25, C_5 = 2.4216061708512208e42;

// struct for the input parameters
typedef struct
{
    REAL8 f, M, eta, delta, e0, D, iota, beta, phic, shft, f0, ff;
} arg_;

// function to calculate ref_phasing
void ref_phasingLeO(arg_ *, REAL8 *);

// function to calculate h+x for a single frequency
void htildeLeO(COMPLEX16 *, COMPLEX16 *, arg_ *, REAL8 *, size_t);

////////////////////////////////////////////////////////////////////
// main function , loop over frequencies
////////////////////////////////////////////////////////////////////
int XLALSimInspiralPerAdvFDLeO(
    COMPLEX16FrequencySeries **hptilde, /**< FD plus polarization */
    COMPLEX16FrequencySeries **hctilde, /**< FD cross polarization */
    const REAL8 phiRef,                 /**< Orbital coalescence phase (rad) */
    const REAL8 deltaF,                 /**< Frequency resolution */
    const REAL8 m1_SI,                  /**< Mass of companion 1 (kg) */
    const REAL8 m2_SI,                  /**< Mass of companion 2 (kg) */
    const REAL8 fStart,                 /**< Start GW frequency (Hz) */
    const REAL8 fEnd,                   /**< Highest GW frequency (Hz): end at Schwarzschild ISCO */
    const REAL8 f_ref,                  /**< Reference GW frequency (Hz) - if 0 reference point is coalescence */
    const REAL8 i,                      /**< Polar inclination of source (rad) */
    const REAL8 r,                      /**< Distance of source (m) */
    const REAL8 inclination_azimuth,    /**< Azimuthal component of inclination angles [0, 2 LAL_PI]*/
    const REAL8 e_min                   /**< Initial eccentricity at frequency f_min: range [0, 0.4] */
)

{

    // below arguments name are defined according the reference mathematica noteboook
    const REAL8 m1 = m1_SI / LAL_MSUN_SI;
    const REAL8 m2 = m2_SI / LAL_MSUN_SI;
    const REAL8 m = m1 + m2;
    arg_ arg;
    arg.phic = phiRef;
    arg.M = m1_SI + m2_SI;
    arg.delta = m1_SI - m2_SI;
    arg.eta = m1 * m2 / (m * m);
    arg.f0 = fStart; /*min frequency in aLIGO*/
    arg.iota = i;
    arg.D = r;
    arg.beta = inclination_azimuth;
    arg.e0 = e_min;                        /*eccentricity at min frequency in aLIGO, fStart*/
    arg.ff = 8.743673908471672e33 / arg.M; /* same as this, arg.ff = C_3/( G*(arg.M)*LAL_PI*pow(6,3/2) ); which is the last stable orbit (lso) frequency. where pow(6.0,3.0/2.0) = 14.696938456699069 */

    LIGOTimeGPS tC = {0, 0};
    XLALGPSAdd(&tC, -1 / deltaF); /* coalesce at t=0 */
    arg.shft = 6.283185307179586 * (tC.gpsSeconds + 1e-9 * tC.gpsNanoSeconds);
    REAL8 f_max;

    /* Perform some initial checks */
    if (!hptilde)
        XLAL_ERROR(XLAL_EFAULT);
    if (*hptilde)
        XLAL_ERROR(XLAL_EFAULT);
    if (!hctilde)
        XLAL_ERROR(XLAL_EFAULT);
    if (*hctilde)
        XLAL_ERROR(XLAL_EFAULT);
    if (m1_SI <= 0)
        XLAL_ERROR(XLAL_EDOM);
    if (m2_SI <= 0)
        XLAL_ERROR(XLAL_EDOM);
    if (fStart <= 0)
        XLAL_ERROR(XLAL_EDOM);
    if (r <= 0)
        XLAL_ERROR(XLAL_EDOM);

    // setting the maximum frequency
    if (fEnd == 0.) // End at ISCO
        f_max = arg.ff;
    else // End at user-specified freq.
        f_max = fEnd;
    if (f_max <= fStart)
        XLAL_ERROR(XLAL_EDOM);

    // some values needed for loop over frequencies
    // I am strictly considering lso frequency as my fEnd.
    size_t nStart, nEnd, n, idx, j;
    n = (size_t)(f_max / deltaF + 1);
    nStart = (size_t)ceil(fStart / deltaF);
    nEnd = n - nStart; /* so, n is an index higher than the index of f_lso */

    // the following pointers are used to push h+ hX values to python
    COMPLEX16FrequencySeries *htilde_p;
    COMPLEX16FrequencySeries *htilde_c;

    htilde_p = XLALCreateCOMPLEX16FrequencySeries("htilde_p: FD waveform", &tC, 0.0, deltaF, &lalStrainUnit, n);
    if (!htilde_p)
        XLAL_ERROR(XLAL_EFUNC);
    memset(htilde_p->data->data, 0, n * sizeof(COMPLEX16));
    XLALUnitDivide(&htilde_p->sampleUnits, &htilde_p->sampleUnits, &lalSecondUnit);

    htilde_c = XLALCreateCOMPLEX16FrequencySeries("htilde_c: FD waveform", &tC, 0.0, deltaF, &lalStrainUnit, n);
    if (!htilde_c)
        XLAL_ERROR(XLAL_EFUNC);
    memset(htilde_c->data->data, 0, n * sizeof(COMPLEX16));
    XLALUnitDivide(&htilde_c->sampleUnits, &htilde_c->sampleUnits, &lalSecondUnit);

    COMPLEX16 *data_p = NULL;
    COMPLEX16 *data_c = NULL;
    data_p = htilde_p->data->data;
    data_c = htilde_c->data->data;

    // get ref_phasing at each harmonics
    arg.f = f_ref;
    REAL8 ref_psi[4];
    ref_phasingLeO(&arg, ref_psi);

    // FILE *ptr = NULL;
    // ptr = fopen("periastronEccFD.txt", "w");

    // loop over frequencies
    // make it loop over decreasing integers that stops at 0. Condition is less here.
    for (j = nEnd; j--;)
    {
        idx = j + nStart;
        arg.f = idx * deltaF;

        htildeLeO(data_p, data_c, &arg, ref_psi, idx);
    }
    // fclose(ptr);

    *hptilde = htilde_p;
    *hctilde = htilde_c;
    return XLAL_SUCCESS;
}

////////////////////////////////////////////////////////////////////////
/* 'int main' for compliling and testing purpose */
////////////////////////////////////////////////////////////////////////
// steps to use int main()
// 0. comment out #include <stdio.h>
// 1. comment out '*hptilde = htilde_p;' and '*hctilde = htilde_c;' in XLALSimInspiralPeriastronEccFD
// 2. change **hptilde to *hptilde, **hctilde to *hctilde in XLALSimInspiralPeriastronEccFD
// 3. comment out
/*#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif*/
// 4. comment out initial checks
// 5. remove commet from 'int main'
// 6. take out the ref phasing
// 6. To print your desire result, remove comment // from desire printf s include in this file or add your desire printf s .
// 7. Run the code with the following commands. But you should link the env for lalsuite or lalsimulation beforehand.
// gcc LALSimInspiralPerAdvFDLeO.c -lm -ldl -l lal
// ./a.out

// int main()
// {
//     const REAL8 phiRef = 1.2;                    /**< Orbital coalescence phase (rad) */
//     const REAL8 deltaF = 10.0;                    /**< Frequency resolution */
//     const REAL8 m1 = 6.5*1.989e30;                     /**< Mass of companion 1 (kg) */
//     const REAL8 m2 = 5.8*1.989e30;                     /**< Mass of companion 2 (kg) */
//     const REAL8 f_min = 10.0;                    /**< Start GW frequency (Hz) */
//     const REAL8 f_max = 30.0;                      /**< Highest GW frequency (Hz): end at Schwarzschild ISCO */
//     const REAL8 f_ref = 10.0;                     /**< Reference GW frequency (Hz) - if 0 reference point is coalescence */
//     const REAL8 inclination = 0.04;                         /**< Polar inclination of source (rad) */
//     const REAL8 distance = 200*3.086e22;                       /**< Distance of source (m) */
//     const REAL8 longAscNodes = 0.1;       /**< Azimuthal component of inclination angles [0, 2 LAL_PI]*/
//     const REAL8 eccentricity = 0.1;                     /**< Initial eccentricity at frequency f_min: range [0, 0.4] */

//     COMPLEX16FrequencySeries *hptilde;
//     COMPLEX16FrequencySeries *hctilde;

//     LIGOTimeGPS tC = {0, 0};
//     size_t n;
//     n = (size_t) (f_max / deltaF + 1);
//     XLALGPSAdd(&tC, -1 / deltaF);

//     hptilde = XLALCreateCOMPLEX16FrequencySeries("htilde_p: FD waveform", &tC, f_min, deltaF, &lalStrainUnit, n);
//     memset(hptilde->data->data, 0, n * sizeof(COMPLEX16));

//     hctilde = XLALCreateCOMPLEX16FrequencySeries("htilde_c: FD waveform", &tC, f_min, deltaF, &lalStrainUnit, n);
//     memset(hptilde->data->data, 0, n * sizeof(COMPLEX16));

//     /* Call the waveform driver routine */
//     int ret;
//     ret = XLALSimInspiralPerAdvFDLeO(hptilde, hctilde, phiRef, deltaF, m1, m2, f_min, f_max, f_ref, inclination, distance,  longAscNodes, eccentricity);

//     return 0;
// }

////////////////////////////////////////////////////////////////////
// function to calculate h+x for a single frequency
////////////////////////////////////////////////////////////////////
// the array of hp and hc values are push in the memory addresses of data_p and data_c respectively.
void htildeLeO(COMPLEX16 *data_p, COMPLEX16 *data_c, arg_ *arg, REAL8 *psip, size_t idx)
{
    const REAL8 M = arg->M;
    const REAL8 ff = arg->ff;
    const REAL8 shft = arg->shft;
    const REAL8 phic = arg->phic;
    const REAL8 iota = arg->iota;
    const REAL8 beta = arg->beta;
    const REAL8 D = arg->D;
    const REAL8 f = arg->f;

    // sine and cosine values for calculation hormonics' (from sincos_ struct)
    // check the function 'harmonics'
    sincos_ sc;
    sc.si = sin(iota);
    sc.si_2 = sc.si * sc.si;
    sc.s2b = sin(2. * beta);
    sc.ci = cos(iota);
    sc.ci_2 = sc.ci * sc.ci;
    sc.c2b = cos(2. * beta);

    /////////////////////// local variables (repetitive variables) ///////////////////////
    // eta values
    const REAL8 eta = arg->eta;
    // et pow. its powers will be calculated after we calculate the its value later.
    REAL8 et, etp2, etp4; /* struct declaration */
    // et0 values and it's powers
    const REAL8 e0_0 = 1., e0_1 = arg->e0, e0_2 = e0_1 * e0_1;
    // harmonics values
    CS_ CS; /* struct declaration */
    // chi values
    chi_struct chi_; /* struct declaration */

    // chi value and its fractional powers are use for calculating the PN coefficients of k (periastron advancement), et (eccentricity at f) and psi (fourier phase).
    // fractional power is slow to calculated. So its better to pre-declare the repetitive values
    // e.g. chi_.p2b3 means chi to the power 2 by 3, or just chi^(2/3)
    REAL8 chi = f / (arg->f0);
    chi_.p1 = chi, chi_.p2b3 = pow(chi, 0.6666666666666666), chi_.p4b3 = pow(chi, 1.3333333333333333), chi_.p7 = pow(chi, 7.), chi_.p19b3 = pow(chi, 6.333333333333333), chi_.p19b9 = pow(chi, 2.111111111111111), chi_.p22b3 = pow(chi, 7.333333333333333), chi_.p23b3 = pow(chi, 7.666666666666667), chi_.p25b9 = pow(chi, 2.7777777777777777), chi_.p28b9 = pow(chi, 3.111111111111111), chi_.p31b9 = pow(chi, 3.4444444444444446), chi_.p38b9 = pow(chi, 4.222222222222222), chi_.p44b9 = pow(chi, 4.888888888888889), chi_.p47b9 = pow(chi, 5.222222222222222), chi_.p50b9 = pow(chi, 5.555555555555555), chi_.p9b2 = pow(chi, 4.5), chi_.p19b6 = pow(chi, 3.1666666666666665), chi_.p19b18 = pow(chi, 1.0555555555555556), chi_.p23b6 = pow(chi, 3.8333333333333335), chi_.p25b6 = pow(chi, 4.166666666666667), chi_.p29b6 = pow(chi, 4.833333333333333), chi_.p31b6 = pow(chi, 5.166666666666667), chi_.p31b18 = pow(chi, 1.7222222222222223), chi_.p37b18 = pow(chi, 2.0555555555555554), chi_.p43b18 = pow(chi, 2.388888888888889), chi_.p49b18 = pow(chi, 2.7222222222222223), chi_.p55b18 = pow(chi, 3.0555555555555554), chi_.p95b18 = pow(chi, 5.277777777777778), chi_.p107b18 = pow(chi, 5.944444444444445), chi_.p113b18 = pow(chi, 6.277777777777778), chi_.p119b18 = pow(chi, 6.611111111111111), chi_.p125b18 = pow(chi, 6.944444444444445), chi_.p131b18 = pow(chi, 7.277777777777778), chi_.p8 = pow(chi, 8.), chi_.p25b3 = pow(chi, 8.333333333333334), chi_.p34b9 = pow(chi, 3.7777777777777777), chi_.p37b9 = pow(chi, 4.111111111111111), chi_.p53b9 = pow(chi, 5.888888888888889), chi_.p56b9 = pow(chi, 6.222222222222222);

    // log values
    // ln_chi appears in 3PN coefficients of et and psi (in functions k_et_psi_PNe0Coeff1, et_psi_PNe0Coeff2, k_psi_PNe0Coeff1, psi_PNe0Coeff2)
    const REAL8 ln_chi = log(chi);
    REAL8 ln_v3;

    // this is for calculating the value x (the PN parameter); needed for k, et and psi calculation
    const REAL8 Gmk = (pow(1.556356800498986e-35 * f * M, 0.6666666666666667));

    // xk is just x for k calculation
    // unit is the unitary function which is zero under some condition; it is dependent on frequency and harmonic indices (l and n here)
    // ln_l or log(l) appers in psi (5/2)PN e0^0 coefficient (in function k_et_psi_PNe0Coeff2)
    // k here is periatron advancement and depend only on l harmonic index
    REAL8 xk, unit, k_[3];
    REAL8 *kp = k_;

    // allowed harmonic indices combination (l,n) for 0PN fourier phase
    // int ln_[7][1] = {{2,-2}}; for e0=0 condition
    int ln_[4][2] = {{1, 0},
                     {1, -2},
                     {2, -2},
                     {3, -2}};

    // ll is l, nn is n; ke(for k), ee(for et) and se(for psi) are e0 cofficients.
    // e0 cofficients  are use for calculating PN coefficients
    REAL8 ll, nn, k, ke[8], ee[6], se[12];

    // ln_x is use for calculating PN coefficients (in function et_psi_PNe0Coeff2, psi_PNe0Coeff2)
    REAL8 x, x_3b2, x_5b2, x_2, x_3, ln_x, k1PN, k2PN, k5b2PN, k3PN, e0PN, e1PN, e3b2PN, e2PN, e5b2PN, e3PN, e3PNh, s0PN, s3b2PN, s1PN, s2PN, s5b2PN, s3PN, s1PNh, s2PNh, s5b2PNh, s3PNh, nd, psi, Amp;
    // hf0_p ( harmonics dependent part of h+); output which gets added every iteration of (l,n). hf0_c is for hx.
    // L_psi common value use for calculating hf0_p and hf0_c
    COMPLEX16 L_Psi = 0. * I, hf0_p = 0. * I, hf0_c = 0. * I, xi_p = 0. * I, xi_c = 0. * I;

    // get all the harmonic_indices_independent PN_coefficients for k, et, psi
    k_et_psi_PNe0Coeff1(ke, ee, se, &chi_, eta, ln_chi);

    /////////////////////////////////////////////////////////
    //////////////// Calculation of k values (periastron advancement parameter) ////////////////
    // PN coefficients for calculating advacement of periastron (harmonic indices independent).
    // ke(for k) are e0 cofficients from the function k_et_psi_PNe0Coeff1.
    k1PN = e0_0 * ke[0] + e0_2 * ke[1];
    k2PN = e0_0 * ke[2] + e0_2 * ke[3];
    k5b2PN = e0_0 * ke[4] + e0_2 * ke[5];
    k3PN = e0_0 * ke[6] + e0_2 * ke[7];

    // harmonic l index values = ii +1 ranging from 1 to 3
    xk = Gmk; /* PN parameter x; pow( 1/((double)(1)) , 2/3 ) = 1. */
    kp[0] = xk * k1PN + xk * xk * k2PN + pow(xk, 2.5) * k5b2PN + xk * xk * xk * k3PN;
    xk = Gmk * 0.6299605249474366; /* PN parameter x; pow( 1/((double)(2)) , 2/3 ) = 0.6299605249474366 */
    kp[1] = xk * k1PN + xk * xk * k2PN + pow(xk, 2.5) * k5b2PN + xk * xk * xk * k3PN;
    xk = Gmk * 0.48074985676913606; /* PN parameter x; pow( 1/((double)(1)) , 2/3 ) = 0.48074985676913606 */
    kp[2] = xk * k1PN + xk * xk * k2PN + pow(xk, 2.5) * k5b2PN + xk * xk * xk * k3PN;

    /////////////////////////////////////////////////////////
    // create 2D pointer array of harmonic idices.
    // ln_[18][3] note the allowed size
    int(*ln_p)[2] = ln_; /* e.g. *(*(ln1p+7)+1) means ln_p[7][1]=-2 in (2,-2) harmonics */

    // PN coefficients for calculating ecentricity (harmonic indices independent).
    // ee(for et) are e0 cofficients from the function k_et_psi_PNe0Coeff1.
    e0PN = e0_1 * ee[0];
    e1PN = e0_1 * ee[1];
    e3b2PN = e0_1 * ee[2];
    e2PN = e0_1 * ee[3];
    e5b2PN = e0_1 * ee[4];
    e3PN = e0_1 * ee[5];

    // PN coefficients for calculating fourier phase (harmonic indices independent).
    // se(for psi) are e0 cofficients from the function k_et_psi_PNe0Coeff1.
    s0PN = e0_0 * se[0] + e0_2 * se[1];
    s3b2PN = e0_0 * se[2] + e0_2 * se[3];
    s1PN = e0_0 * se[4] + e0_2 * se[5];
    s2PN = e0_0 * se[6] + e0_2 * se[7];
    s5b2PN = e0_0 * se[8] + e0_2 * se[9];
    s3PN = e0_0 * se[10] + e0_2 * se[11];

    // to find all possible l and n combination , 2-D ln_[18][3]
    // this 'for loop' is designed to avoid double iteration over l and n
    size_t ii;
    for (ii = 0; ii < 4; ii++)
    {
        ll = (REAL8) * (*(ln_p + ii));
        nn = (REAL8) * (*(ln_p + ii) + 1);
        k = kp[(int)ll - 1];

        // unitary function
        // some terms due to harmonic dependent frequencies are cut off above lso frequency
        unit = (ll - (ll + nn) * (k / (1. + k))) * ff - 2. * f;
        // certain harmonic components will be omitted according to the following condition
        if (unit >= 0.)
        {
            // PN parameter and its powers
            x = Gmk * pow(1 / fabs(ll - (ll + nn) * k / (1 + k)), 0.6666666666666667);
            x_3b2 = pow(x, 1.5);
            x_5b2 = pow(x, 2.5);
            x_2 = x * x;
            x_3 = x * x * x;
            ln_x = log(x);

            ln_v3 = log(sqrt(x_3));

            // get all the harmonic indices dedependent PN coefficients for et, psi
            et_psi_PNe0Coeff2(ee, se, &chi_, eta, ll, nn, ln_x, ln_v3);

            /////////////////////////////////////////////////////////
            /////////////////////// eccentricity at a frequency f ///////////////////////
            // PN coefficient for calculating ecentricity (harmonic indices dependent terms).
            // ee(for et) are e0 cofficients from the function et_psi_PNe0Coeff2.
            e3PNh = e3PN + (e0_1 * ee[5]);

            // powers of eccentricity (will be use in the function 'harmonics' to calculate xi. xi is the harmonic dependent part of the amplitude)
            et = e0PN + x * e1PN + x_3b2 * e3b2PN + x_2 * e2PN + x_5b2 * e5b2PN + x_3 * e3PNh;
            etp2 = et * et;
            etp4 = etp2 * etp2;

            /////////////////////////////////////////////////////////
            /////////////////////// fourier phase at a frequency f ///////////////////////
            // we are still inside the for loop of l and n
            // PN coefficients for calculating fourier phase (harmonic indices dependent terms).
            // se(for psi) are e0 cofficients from the function et_psi_PNe0Coeff2.
            s1PNh = s1PN + e0_0 * se[4] + e0_2 * se[5];
            s2PNh = s2PN + e0_0 * se[6] + e0_2 * se[7];
            s5b2PNh = s5b2PN + e0_0 * se[8] + e0_2 * se[9];
            s3PNh = s3PN + e0_0 * se[10] + e0_2 * se[11];

            psi = 1. / (256. * x_5b2 * eta) * 3. * ll * (s0PN + x_3b2 * s3b2PN + x * s1PNh + x_2 * s2PNh + x_5b2 * s5b2PNh + x_3 * s3PNh);

            // ref_phasing = psip[cc] . shift relates to time of coalescence tc .
            psi = psi + f * shft - (ll - (ll + nn) * k / (1 + k)) * phic - psip[ii];

            // get the harmonic components: Cp,Cx,Sp,Sx needed for calculating xi
            harmonics(ii, &CS, &sc, et);

            ////////// xi_plus and xi_cross 0PN //////////
            nd = pow(1. - etp2, 1.75) / pow(1. + 3.0416666666666665 * etp2 + 0.385417 * etp4, 0.5);
            Xi_PlusCross(CS.Cp, CS.Sp, CS.Cx, CS.Sx, &xi_p, &xi_c, nd);

            // FOR TESTING PURPOSE with 'int main'
            // if(f==30.0)
            //     //fprintf(ptr, "f=%f, k[%d]=%f, et[%d,%d]=%f, psiref[%d,%d]=%f, psi[%d,%d]=%f \n", f,(int)ll,k,(int)ll,(int)nn,et.p1,(int)ll,(int)nn,psip[ii],(int)ll,(int)nn,psi);
            //     printf("f=%f, k[%d]=%f, et[%d,%d]=%f, psiref[%d,%d]=%f, psi[%d,%d]=%f, nd=%f, xi[%d,%d]=%e+i%e \n", f,(int)ll,k,(int)ll,(int)nn,et.p1,(int)ll,(int)nn,psip[ii],(int)ll,(int)nn,psi, nd, (int)ll,(int)nn,creal(xi_p),cimag(xi_p));

            //////////// 0PN /////////////
            // L_Psi = cexp( -I*(-0.7853981633974483 + psi) ); /* in terms of complex exponent, Pi/4=0.7853981633974483*/
            L_Psi = (cos(psi - 0.7853981633974483) - I * sin(psi - 0.7853981633974483));

            hf0_p = hf0_p + xi_p * 0.6299605249474366 * pow(ll, 0.6666666666666666) * L_Psi;
            hf0_c = hf0_c + xi_c * 0.6299605249474366 * pow(ll, 0.6666666666666666) * L_Psi;
        }
    }

    // I am using the same 0PN amplitude given in TaylorF2 code
    // Amplitude = 0.05319687753491586*(pow(mchirp,0.8333333333333334)/D)*LAL_MRSUN_SI/LAL_MTSUN_SI*pow(f,-1.1666666666666667); /*np.sqrt(5./384.)*pow(np.pi, -2./3.) = 0.05319687753491586*/
    Amp = (3.396106183489543e-23 * sqrt(eta) * pow(M, 0.8333333333333334)) / D * pow(f, -1.1666666666666667);

    // the final values of complex plus and cross polarization for a that particular frequency are pushed in the following pointers.
    data_p[idx] = Amp * hf0_p;
    data_c[idx] = Amp * hf0_c;
    // printf("f = %f, Amp = %e, h+ = %e+i*%e\n", f, Amp, creal(data_p[idx]),cimag(data_p[idx]));
    // printf("f = %f, Amp = %e, hx = %e+i*%e\n", f, Amp, creal(data_c[idx]),cimag(data_c[idx]));
}

////////////////////////////////////////////////////////////////////
// function to calculate phasing wrt to reference frequency.
// similar line comments from 'htilde' are ommited to save spaces.
// code structure of 'ref_phasing' is same as 'htilde'. But here we are only interested in phasing at ref_frequrncy.
// unlike taylorF2 ref_phasing shows harmonic structure.
////////////////////////////////////////////////////////////////////
void ref_phasingLeO(arg_ *arg, REAL8 *psip)
{
    const REAL8 M = arg->M;
    const REAL8 ff = arg->ff;
    const REAL8 f = arg->f;

    const REAL8 eta = arg->eta;

    const REAL8 e0_0 = 1., e0_1 = arg->e0, e0_2 = e0_1 * e0_1;

    chi_struct chi_;

    REAL8 chi = f / (arg->f0);
    chi_.p1 = chi, chi_.p2b3 = pow(chi, 0.6666666666666666), chi_.p4b3 = pow(chi, 1.3333333333333333), chi_.p7 = pow(chi, 7.), chi_.p19b3 = pow(chi, 6.333333333333333), chi_.p19b9 = pow(chi, 2.111111111111111), chi_.p22b3 = pow(chi, 7.333333333333333), chi_.p23b3 = pow(chi, 7.666666666666667), chi_.p25b9 = pow(chi, 2.7777777777777777), chi_.p28b9 = pow(chi, 3.111111111111111), chi_.p31b9 = pow(chi, 3.4444444444444446), chi_.p38b9 = pow(chi, 4.222222222222222), chi_.p44b9 = pow(chi, 4.888888888888889), chi_.p47b9 = pow(chi, 5.222222222222222), chi_.p50b9 = pow(chi, 5.555555555555555), chi_.p9b2 = pow(chi, 4.5), chi_.p19b6 = pow(chi, 3.1666666666666665), chi_.p19b18 = pow(chi, 1.0555555555555556), chi_.p23b6 = pow(chi, 3.8333333333333335), chi_.p25b6 = pow(chi, 4.166666666666667), chi_.p29b6 = pow(chi, 4.833333333333333), chi_.p31b6 = pow(chi, 5.166666666666667), chi_.p31b18 = pow(chi, 1.7222222222222223), chi_.p37b18 = pow(chi, 2.0555555555555554), chi_.p43b18 = pow(chi, 2.388888888888889), chi_.p49b18 = pow(chi, 2.7222222222222223), chi_.p55b18 = pow(chi, 3.0555555555555554), chi_.p95b18 = pow(chi, 5.277777777777778), chi_.p107b18 = pow(chi, 5.944444444444445), chi_.p113b18 = pow(chi, 6.277777777777778), chi_.p119b18 = pow(chi, 6.611111111111111), chi_.p125b18 = pow(chi, 6.944444444444445), chi_.p131b18 = pow(chi, 7.277777777777778), chi_.p8 = pow(chi, 8.), chi_.p25b3 = pow(chi, 8.333333333333334), chi_.p34b9 = pow(chi, 3.7777777777777777), chi_.p37b9 = pow(chi, 4.111111111111111), chi_.p53b9 = pow(chi, 5.888888888888889), chi_.p56b9 = pow(chi, 6.222222222222222);

    const REAL8 ln_chi = log(chi);
    REAL8 ln_v3;

    const REAL8 Gmk = (pow(1.556356800498986e-35 * f * M, 0.6666666666666667));

    REAL8 xk, unit, k_[3];
    REAL8 *kp = k_;

    int ln_[4][2] = {{1, 0},
                     {1, -2},
                     {2, -2},
                     {3, -2}};

    REAL8 ll, nn, k, ke[8], se[12];
    REAL8 x, x_3b2, x_5b2, x_2, x_3, ln_x, k1PN, k2PN, k5b2PN, k3PN, s0PN, s3b2PN, s1PN, s2PN, s5b2PN, s3PN, s1PNh, s2PNh, s5b2PNh, s3PNh;

    k_psi_PNe0Coeff1(ke, se, &chi_, eta, ln_chi);

    /////////////////////////////////////////////////////////
    //////////////// Calculation of k values ////////////////
    k1PN = e0_0 * ke[0] + e0_2 * ke[1];
    k2PN = e0_0 * ke[2] + e0_2 * ke[3];
    k5b2PN = e0_0 * ke[4] + e0_2 * ke[5];
    k3PN = e0_0 * ke[6] + e0_2 * ke[7];

    // harmonic l index values = ii +1 ranging from 1 to 3
    xk = Gmk; /* PN parameter x; pow( 1/((double)(1)) , 2/3 ) = 1. */
    kp[0] = xk * k1PN + xk * xk * k2PN + pow(xk, 2.5) * k5b2PN + xk * xk * xk * k3PN;
    xk = Gmk * 0.6299605249474366; /* PN parameter x; pow( 1/((double)(2)) , 2/3 ) = 0.6299605249474366 */
    kp[1] = xk * k1PN + xk * xk * k2PN + pow(xk, 2.5) * k5b2PN + xk * xk * xk * k3PN;
    xk = Gmk * 0.48074985676913606; /* PN parameter x; pow( 1/((double)(1)) , 2/3 ) = 0.48074985676913606 */
    kp[2] = xk * k1PN + xk * xk * k2PN + pow(xk, 2.5) * k5b2PN + xk * xk * xk * k3PN;

    /////////////////////////////////////////////////////////
    int(*ln_p)[2] = ln_; /* e.g. *(*(ln1p+7)+1) means ln_p[7][1]=-2 in (2,-2) harmonics */

    s0PN = e0_0 * se[0] + e0_2 * se[1];
    s3b2PN = e0_0 * se[2] + e0_2 * se[3];
    s1PN = e0_0 * se[4] + e0_2 * se[5];
    s2PN = e0_0 * se[6] + e0_2 * se[7];
    s5b2PN = e0_0 * se[8] + e0_2 * se[9];
    s3PN = e0_0 * se[10] + e0_2 * se[11];

    size_t ii;
    for (ii = 0; ii < 4; ii++)
    {
        ll = (REAL8) * (*(ln_p + ii));
        nn = (REAL8) * (*(ln_p + ii) + 1);
        // k = 0.;
        k = kp[(int)ll - 1];

        // unitary function
        unit = (ll - (ll + nn) * (k / (1. + k))) * ff - 2. * f;
        if (unit >= 0.)
        {
            // PN parameter and its powers
            x = Gmk * pow(1 / fabs(ll - (ll + nn) * k / (1 + k)), 0.6666666666666667);
            x_3b2 = pow(x, 1.5);
            x_5b2 = pow(x, 2.5);
            x_2 = x * x;
            x_3 = x * x * x;
            ln_x = log(x);
            ln_v3 = log(sqrt(x_3));

            psi_PNe0Coeff2(se, &chi_, eta, ll, nn, ln_x, ln_v3);

            /////////////////////////////////////////////////////////
            /////////////////////// fourier phase at a frequency f ///////////////////////
            s1PNh = s1PN + e0_0 * se[4] + e0_2 * se[5];
            s2PNh = s2PN + e0_0 * se[6] + e0_2 * se[7];
            s5b2PNh = s5b2PN + e0_0 * se[8] + e0_2 * se[9];
            s3PNh = s3PN + e0_0 * se[10] + e0_2 * se[11];

            psip[ii] = 1. / (256. * x_5b2 * eta) * 3. * ll * (s0PN + x_3b2 * s3b2PN + x * s1PNh + x_2 * s2PNh + x_5b2 * s5b2PNh + x_3 * s3PNh);
        }
        else
        {
            psip[ii] = 0.0;
        }
    }
}