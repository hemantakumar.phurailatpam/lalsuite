/*
 *  Copyright (C) 2022 Phurailatpam Hemantakumar
 *  Assembled from code found in:
 *    - LALSimInspiralTaylorF2Ecc.c
 *    - LALSimInspiralEccentricityFD.c
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA  02110-1301  USA
 */

// #include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <lal/LALConstants.h>
#include <lal/LALAtomicDatatypes.h>

#ifndef _OPENMP1
#define omp1 ignore
#endif


// Harmonics' variable. This struct is use in the function 'harmonics'.
typedef struct
{
    REAL8 Cp, Sp, Cx, Sx;
} CS_; /* Cp,Cx,Sp,Sx needed for calculating xi */
// sine cosine values needed for calculatng harmonics.
// s2b stands for sin(2*beta), and ci_2 stands for pow(cos(iota), 2) .
typedef struct
{
    REAL8 si, ci, s2b, c2b, si_2, ci_2;
} sincos_;

// xi is the harmonics dependent amplitude. p and c stands for plus and cross polarization respectively.
typedef struct
{
    COMPLEX16 xi_p, xi_c;
} xi_;

// chi power values. p stands for power.
// these values are use for calculating the PN coefficients of k (periastron advancement), et (eccentricity at f) and psi (fourier phase).
typedef struct
{
    REAL8 p1, p2b3, p4b3, p7, p19b3, p19b9, p22b3, p23b3, p25b9, p28b9, p31b9, p38b9, p44b9, p47b9, p50b9, p9b2, p19b6, p19b18, p23b6, p25b6, p29b6, p31b6, p31b18, p37b18, p43b18, p49b18, p55b18, p95b18, p107b18, p113b18, p119b18, p125b18, p131b18, p8, p25b3, p34b9, p37b9, p53b9, p56b9;
} chi_struct;

////////////////////////////////////////////////////////////////////
// early declaration of functions
////////////////////////////////////////////////////////////////////
// // function to calculate the harmonics for the given j and n values
// void harmonics( int, CS_*, sincos_*, eccn* );
// void Xi_PlusCross( CS_*, eccn*, xi_* ); /*function for calculating xi (harmonics dependent amplitude) */
// // function to calculate PN coefficients for k (periastron advancement), et (eccentricity at f) and psi (fourier phase).
// void k_et_psi_PNe0Coeff1( REAL8*, REAL8*, REAL8*, chi_struct*, REAL8, REAL8 ); /* Harmonic indices independent */
// void et_psi_PNe0Coeff2( REAL8*, REAL8*, chi_struct*, REAL8, REAL8, REAL8, REAL8, REAL8 ); /* Harmonic indices dependent */
// // for ref phasing clasulation
// void k_psi_PNe0Coeff1( REAL8*, REAL8*, chi_struct*, REAL8, REAL8 ); /* Harmonic indices independent */
// void psi_PNe0Coeff2( REAL8*, chi_struct*, REAL8, REAL8, REAL8, REAL8, REAL8 ); /* Harmonic indices dependent */


// function to calculate the harmonics for the given j and n values
void harmonics_( CS_* CS, sincos_* sc);
void Xi_PlusCross_( REAL8 Cp_p, REAL8 Sp_p, REAL8 Cx_p, REAL8 Sx_p, COMPLEX16 *xi_p, COMPLEX16 *xi_c, REAL8 nd); /*function for calculating xi (harmonics dependent amplitude) */
// function to calculate PN coefficients for k (periastron advancement), et (eccentricity at f) and psi (fourier phase).
void et_psi_PNe0Coeff1_(REAL8 *ee, REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ln_chi); /* Harmonic indices independent */
void et_psi_PNe0Coeff2_( REAL8 *ee, REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ll, REAL8 nn, REAL8 ln_x, REAL8 ln_v3 ); /* Harmonic indices dependent */
// for ref phasing clasulation
void psi_PNe0Coeff1_(REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ln_chi); /* Harmonic indices independent */
void psi_PNe0Coeff2_( REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ll, REAL8 nn, REAL8 ln_x, REAL8 ln_v3 ); /* Harmonic indices dependent */


///////////////////////////////////////////////////////////////////////////////////////
////////////////////////////// VARIOUS FUNCTIONS //////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////
// 0PN harmoics
////////////////////////////////////////////////////////////////////
void harmonics_( CS_* CS, sincos_* sc)
{
    CS->Cp = -2.*sc->c2b-2.*sc->c2b*sc->ci_2;
    CS->Sp = -2.*sc->s2b-2.*sc->ci_2*sc->s2b;
    CS->Cx = 4.*sc->ci*sc->s2b;
    CS->Sx = -4.*sc->c2b*sc->ci;

}

////////////////////////////////////////////////////////////////////
// function to find xi
// corresspond to function xi in Mathematica notebook. 
////////////////////////////////////////////////////////////////////
void Xi_PlusCross_( REAL8 Cp_p, REAL8 Sp_p, REAL8 Cx_p, REAL8 Sx_p, COMPLEX16 *xi_p, COMPLEX16 *xi_c, REAL8 nd)
{
    REAL8 Gamma_l, Sigma_l, al, phil;
    
    ///////////////////////////////////
    //////////// plus /////////////
    ///////////////////////////////////
    Gamma_l = Cp_p;
    Sigma_l = Sp_p;

    al = copysignf( 1.0, Gamma_l )*sqrt( pow(Gamma_l,2.) + pow(Sigma_l,2.) ); /* alpha_l */

    // to avoid division by 0.
    if(Gamma_l==0)
    {
        phil = -copysign(1.0,Sigma_l) * 1.5707963267948966; /*Pi_b2 = 1.5707963267948966*/
    }
    else
    {
        phil = atan(- (Sigma_l/Gamma_l));
    }
        
    *xi_p = nd * al * ( cos(phil) - I*sin(phil) );

    ////////////////////////////////////
    //////////// 0PN cross /////////////
    ////////////////////////////////////
    Gamma_l = Cx_p;
    Sigma_l = Sx_p;

    al = copysignf( 1.0, Gamma_l )*sqrt( pow(Gamma_l,2.) + pow(Sigma_l,2.) );

    if(Gamma_l==0)
    {
        phil = -copysign(1.0,Sigma_l) * 1.5707963267948966; /*Pi_b2 = 1.5707963267948966*/
    }
    else
    {
        phil = atan(- (Sigma_l/Gamma_l));
    }
        
    *xi_c = nd * al * ( cos(phil) - I*sin(phil) );

}

////////////////////////////////////////////////////////////////////
// function to calculate PN coefficients for k (periastron advancement), et (eccentricity at f) and psi (fourier phase).
// Harmonic indices independent
////////////////////////////////////////////////////////////////////
void et_psi_PNe0Coeff1_(REAL8 *ee, REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ln_chi)
{
    REAL8 eta_2 = eta*eta, eta_3 = eta_2*eta;
    //////////////////////////////////
    /////// et 0PN Coefficients //////
    ee[0]= (1/chi->p19b18); /* et0PN_et0_1_Coeff */

    /////// et 1PN Coefficients //////
    ee[1] = (1.4052579365079365 - 2.736111111111111*eta)/chi->p31b18 + (-1.4052579365079365 + 2.736111111111111*eta)/chi->p19b18; /* et1PN_et0_1_Coeff */

    /////// et 3b2PN Coefficients //////
    ee[2] = ((377.*3.141592653589793)/(144.*chi->p37b18)-(377.*3.141592653589793)/(144.*chi->p19b18)); /* et3b2PN_et0_1_Coeff */

    /////// et 2PN Coefficients //////
    ee[3] = (-1.9747498681185438 + 7.689883708112874*eta - 7.486304012345679*eta_2)/chi->p31b18 + (-1.183105878829155 + 0.18990437610229277*eta + 3.2610918209876543*eta_2)/chi->p43b18 + (3.1578557469476984 - 7.879788084215167*eta + 4.2252121913580245*eta_2)/chi->p19b18; /* et2PN_et0_1_Coeff */

    /////// et 5b2PN Coefficients //////
    ee[4] = (1.6856863474839032 - 43.25439269500695*eta)/chi->p49b18 + (21.430424759029936 - 1.7538914078982104*eta)/chi->p19b18 + (-11.558055553256917 + 22.50414205145258*eta)/chi->p31b18 + (-11.558055553256917 + 22.50414205145258*eta)/chi->p37b18; /* et5b2PN_et0_1_Coeff */

    /////// et 3PN Coefficients //////
    ee[5] = - 7.935552146171627/chi->p19b18 + 4.437601850745452/chi->p31b18 - 67.64838946385173/chi->p37b18 + 1.6625689259538672/chi->p43b18 + 69.48377083332403/chi->p55b18 + (35.4145302358222*eta)/chi->p19b18 - (19.71337893985371*eta)/chi->p31b18 - (3.503973772380662*eta)/chi->p43b18 - (12.19717752358783*eta)/chi->p55b18 - (17.177287477571404*eta_2)/chi->p19b18 + (27.497488695758015*eta_2)/chi->p31b18 - (4.063075689621914*eta_2)/chi->p43b18 - (6.257125528564693*eta_2)/chi->p55b18 + (4.754564882687471*eta_3)/chi->p19b18 - (11.560650023576816*eta_3)/chi->p31b18 + (8.922709565757886*eta_3)/chi->p43b18 - (2.1166244248685415*eta_3)/chi->p55b18 + (4.387566137566138*ln_chi)/chi->p55b18; /* et3PN_et0_1_Coeff */

    //////////////////////////////////
    /////// psi 0PN Coefficients //////
    se[0] = 1.; /* psi0PN_et0_0_Coeff */

    se[1] = -1.6108071135430917/chi->p19b9; /* psi0PN_et0_2_Coeff */ 

    /////// psi 3b2PN Coefficients //////
    se[2] = -50.26548245743669; /* psi3b2PN_et0_0_Coeff */

    se[3] = 50.48185195147069/chi->p19b9 - 26.49733920048539/chi->p28b9; /* psi3b2PN_et0_2_Coeff */

    /////// psi 1PN Coefficients //////
    se[4] = - 3.4193121693121693 + 6.111111111111111*eta; /* psi1PN_et0_0_Coeff */

    se[5] = 4.6174595113029495/chi->p19b9 - 4.5271989609797405/chi->p25b9 - (10.325370012870012*eta)/chi->p19b9 + (8.814694482444141*eta)/chi->p25b9; /* psi1PN_et0_2_Coeff */

    /////// psi 2PN Coefficients //////
    se[6] = - 96.10716450932226 + 43.85912698412698*eta + 42.84722222222222*eta_2; /* psi2PN_et0_0_Coeff */

    se[7] = 56.34521097966795/chi->p19b9 + 12.977443249525058/chi->p25b9 + 0.6305695963516759/chi->p31b9 - (11.02099990468567*eta)/chi->p19b9 - (54.287380863896644*eta)/chi->p25b9 + (11.775120739510275*eta)/chi->p31b9 - (36.277272048441404*eta_2)/chi->p19b9 + (56.50271923709424*eta_2)/chi->p25b9 - (22.564971563560825*eta_2)/chi->p31b9; /* psi2PN_et0_2_Coeff */

    /////// psi 5b2PN Coefficients //////
    se[8] = 60.06010399779534 - 22.689280275926283*eta; /* psi5b2PN_et0_0_Coeff */

    se[9] = - 210.61264554948588/chi->p19b9 + 141.88004620884573/chi->p25b9 + 75.95595393565333/chi->p28b9 - 42.66622732728404/chi->p34b9 + (216.02141641228727*eta)/chi->p19b9 - (276.2479120677702*eta)/chi->p25b9 - (169.84953027662291*eta)/chi->p28b9 + (211.84863109153514*eta)/chi->p34b9; /* psi5b2PN_et0_2_Coeff */

    /////// psi 3PN Coefficients //////
    se[10] = 229.55467539143297 - 4322.017571041981*eta + 63.80497685185184*eta_2 - 98.6304012345679*eta_3; /* psi3PN_et0_0_Coeff */

    se[11] = - 61.572441713118685/chi->p19b9 + 158.359109826785/chi->p25b9 + 830.4127436366826/chi->p28b9 - 1.8075594251680198/chi->p31b9 - 327.46225593097415/chi->p37b9 - (843.1840289315528*eta)/chi->p19b9 - (339.30821080736393*eta)/chi->p25b9 - (29.71198627806088*eta)/chi->p31b9 + (28.006148883848002*eta)/chi->p37b9 - (92.88746834092433*eta_2)/chi->p19b9 - (41.648488332329705*eta_2)/chi->p25b9 + (140.16285329998192*eta_2)/chi->p31b9 + (7.068382176074206*eta_2)/chi->p37b9 - (92.75010314857681*eta_3)/chi->p19b9 + (198.5172942650821*eta_3)/chi->p25b9 - (144.6428183515857*eta_3)/chi->p31b9 + (35.5644754417582*eta_3)/chi->p37b9 - (14.135045491064645*ln_chi)/chi->p37b9; /* psi3PN_et0_2_Coeff */

}


////////////////////////////////////////////////////////////////////
// function to calculate PN coefficients for et (eccentricity at f) and psi (fourier phase).
// Harmonic indices dependent
////////////////////////////////////////////////////////////////////
void et_psi_PNe0Coeff2_( REAL8 *ee, REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ll, REAL8 nn, REAL8 ln_x, REAL8 ln_v3 )
{
    REAL8 eta_2 = eta*eta;
    //////////////////////////////////
    /////// et 3PN Coefficients //////
    ee[5] = (6.581349206349206/chi->p19b18 - 6.581349206349205/chi->p55b18)*ln_x; /* et3PN_et0_1_Coeff */

    //////////////////////////////////
    /////// psi 1PN Coefficients //////
    se[4] = -(8.333333333333332*nn)/ll; /* psi1PN_et0_0_Coeff */

    se[5] = (10.494186046511627*nn)/(chi->p19b9*ll); /* psi1PN_et0_2_Coeff */

    /////// psi 2PN Coefficients //////
    se[6] = (( - 126.21031746031746 - 10.*eta)*nn)/ll; /* psi2PN_et0_0_Coeff */

    se[7] = ((64.19878487102324/chi->p19b9 + 29.494076458102615/chi->p25b9 + (28.998243751150728*eta)/chi->p19b9 - (57.42651808785529*eta)/chi->p25b9)*nn)/ll; /* psi2PN_et0_2_Coeff */

    /////// psi 5b2PN Coefficients //////
    se[8] =   - 6.960539278786909*ln_v3 - 22.689280275926283*eta*ln_v3 + (( - 100.53096491487338 - 167.5516081914556*ln_v3)*nn)/ll; /* psi5b2PN_et0_0_Coeff */

    se[9] = (( - 331.6374815048508/chi->p19b9 + 172.62650814583742/chi->p28b9)*nn)/ll; /* psi5b2PN_et0_2_Coeff */

    /////// psi 3PN Coefficients //////
    se[10] = -163.04761904761904*ln_x + ((507.8074314216428 - 1013.6970233660937*eta + 19.791666666666664*eta_2)*nn)/ll; /* psi3PN_et0_0_Coeff */

    se[11] = ( - 21.855386904761904/chi->p19b9 + 21.202568236596964/chi->p37b9)*ln_x + ((222.04259821895513/chi->p19b9 + 180.43170390834212/chi->p25b9 - 4.108073898949294/chi->p31b9 - (309.52963269393405*eta)/chi->p19b9 - (269.8099928586847*eta)/chi->p25b9 - (76.71328660124686*eta)/chi->p31b9 + (42.79468040085441*eta_2)/chi->p19b9 - (158.6848338604637*eta_2)/chi->p25b9 + (147.00767567470572*eta_2)/chi->p31b9)*nn)/ll; /* psi3PN_et0_2_Coeff */

}


////////////////////////////////////////////////////////////////////
// for reference phasing
// function to calculate PN coefficients for k (periastron advancement) and psi (fourier phase).
// Harmonic indices independent
////////////////////////////////////////////////////////////////////
void psi_PNe0Coeff1_(REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ln_chi)
{
    REAL8 eta_2 = eta*eta, eta_3 = eta_2*eta;
    //////////////////////////////////
    /////// psi 0PN Coefficients //////
    se[0] = 1.; /* psi0PN_et0_0_Coeff */

    se[1] = -1.6108071135430917/chi->p19b9; /* psi0PN_et0_2_Coeff */ 

    /////// psi 3b2PN Coefficients //////
    se[2] = -50.26548245743669; /* psi3b2PN_et0_0_Coeff */

    se[3] = 50.48185195147069/chi->p19b9 - 26.49733920048539/chi->p28b9; /* psi3b2PN_et0_2_Coeff */
    
    /////// psi 1PN Coefficients //////
    se[4] = - 3.4193121693121693 + 6.111111111111111*eta; /* psi1PN_et0_0_Coeff */

    se[5] = 4.6174595113029495/chi->p19b9 - 4.5271989609797405/chi->p25b9 - (10.325370012870012*eta)/chi->p19b9 + (8.814694482444141*eta)/chi->p25b9; /* psi1PN_et0_2_Coeff */

    /////// psi 2PN Coefficients //////
    se[6] = - 96.10716450932226 + 43.85912698412698*eta + 42.84722222222222*eta_2; /* psi2PN_et0_0_Coeff */

    se[7] = 56.34521097966795/chi->p19b9 + 12.977443249525058/chi->p25b9 + 0.6305695963516759/chi->p31b9 - (11.02099990468567*eta)/chi->p19b9 - (54.287380863896644*eta)/chi->p25b9 + (11.775120739510275*eta)/chi->p31b9 - (36.277272048441404*eta_2)/chi->p19b9 + (56.50271923709424*eta_2)/chi->p25b9 - (22.564971563560825*eta_2)/chi->p31b9; /* psi2PN_et0_2_Coeff */

    /////// psi 5b2PN Coefficients //////
    se[8] = 60.06010399779534 - 22.689280275926283*eta; /* psi5b2PN_et0_0_Coeff */

    se[9] = - 210.61264554948588/chi->p19b9 + 141.88004620884573/chi->p25b9 + 75.95595393565333/chi->p28b9 - 42.66622732728404/chi->p34b9 + (216.02141641228727*eta)/chi->p19b9 - (276.2479120677702*eta)/chi->p25b9 - (169.84953027662291*eta)/chi->p28b9 + (211.84863109153514*eta)/chi->p34b9; /* psi5b2PN_et0_2_Coeff */

    /////// psi 3PN Coefficients //////
    se[10] = 229.55467539143297 - 4322.017571041981*eta + 63.80497685185184*eta_2 - 98.6304012345679*eta_3; /* psi3PN_et0_0_Coeff */

    se[11] = - 61.572441713118685/chi->p19b9 + 158.359109826785/chi->p25b9 + 830.4127436366826/chi->p28b9 - 1.8075594251680198/chi->p31b9 - 327.46225593097415/chi->p37b9 - (843.1840289315528*eta)/chi->p19b9 - (339.30821080736393*eta)/chi->p25b9 - (29.71198627806088*eta)/chi->p31b9 + (28.006148883848002*eta)/chi->p37b9 - (92.88746834092433*eta_2)/chi->p19b9 - (41.648488332329705*eta_2)/chi->p25b9 + (140.16285329998192*eta_2)/chi->p31b9 + (7.068382176074206*eta_2)/chi->p37b9 - (92.75010314857681*eta_3)/chi->p19b9 + (198.5172942650821*eta_3)/chi->p25b9 - (144.6428183515857*eta_3)/chi->p31b9 + (35.5644754417582*eta_3)/chi->p37b9 - (14.135045491064645*ln_chi)/chi->p37b9; /* psi3PN_et0_2_Coeff */

}


////////////////////////////////////////////////////////////////////
// for reference phasing
// function to calculate PN coefficients for psi (fourier phase).
// Harmonic indices dependent
////////////////////////////////////////////////////////////////////
void psi_PNe0Coeff2_( REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ll, REAL8 nn, REAL8 ln_x, REAL8 ln_v3 )
{
    REAL8 eta_2 = eta*eta;
    //////////////////////////////////
    /////// psi 1PN Coefficients //////
    se[4] = -(8.333333333333332*nn)/ll; /* psi1PN_et0_0_Coeff */

    se[5] = (10.494186046511627*nn)/(chi->p19b9*ll); /* psi1PN_et0_2_Coeff */

    /////// psi 2PN Coefficients //////
    se[6] = (( - 126.21031746031746 - 10.*eta)*nn)/ll; /* psi2PN_et0_0_Coeff */

    se[7] = ((64.19878487102324/chi->p19b9 + 29.494076458102615/chi->p25b9 + (28.998243751150728*eta)/chi->p19b9 - (57.42651808785529*eta)/chi->p25b9)*nn)/ll; /* psi2PN_et0_2_Coeff */

    /////// psi 5b2PN Coefficients //////
    se[8] =   - 6.960539278786909*ln_v3 - 22.689280275926283*eta*ln_v3 + (( - 100.53096491487338 - 167.5516081914556*ln_v3)*nn)/ll; /* psi5b2PN_et0_0_Coeff */

    se[9] = (( - 331.6374815048508/chi->p19b9 + 172.62650814583742/chi->p28b9)*nn)/ll; /* psi5b2PN_et0_2_Coeff */

    /////// psi 3PN Coefficients //////
    se[10] = -163.04761904761904*ln_x + ((507.8074314216428 - 1013.6970233660937*eta + 19.791666666666664*eta_2)*nn)/ll; /* psi3PN_et0_0_Coeff */

    se[11] = ( - 21.855386904761904/chi->p19b9 + 21.202568236596964/chi->p37b9)*ln_x + ((222.04259821895513/chi->p19b9 + 180.43170390834212/chi->p25b9 - 4.108073898949294/chi->p31b9 - (309.52963269393405*eta)/chi->p19b9 - (269.8099928586847*eta)/chi->p25b9 - (76.71328660124686*eta)/chi->p31b9 + (42.79468040085441*eta_2)/chi->p19b9 - (158.6848338604637*eta_2)/chi->p25b9 + (147.00767567470572*eta_2)/chi->p31b9)*nn)/ll; /* psi3PN_et0_2_Coeff */

}
