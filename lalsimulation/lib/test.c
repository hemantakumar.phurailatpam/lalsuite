/*
 *  Copyright (C) 2007 Jolien Creighton, B.S. Sathyaprakash, Thomas Cokelaer
 *  Copyright (C) 2012 Leo Singer, Evan Ochsner, Les Wade, Alex Nitz
 *  Assembled from code found in:
 *    - LALInspiralStationaryPhaseApproximation2.c
 *    - LALInspiralChooseModel.c
 *    - LALInspiralSetup.c
 *    - LALSimInspiralTaylorF2ReducedSpin.c
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA  02110-1301  USA
 */

// #include <stdio.h>

#include <stdlib.h>
#include <math.h>
#include <lal/Date.h>
#include <lal/FrequencySeries.h>
#include <lal/LALConstants.h>
#include <lal/Sequence.h>
#include <lal/LALDatatypes.h>
#include <lal/LALSimInspiralEOS.h>
//#include <lal/LALSimInspiral.h>
#include <lal/Units.h>
#include <lal/XLALError.h>
#include <lal/AVFactories.h>
#include "testcoeff.c"

// #ifndef _OPENMP
// #define omp ignore
// #endif

/**
 * @addtogroup LALSimInspiralTaylorXX_c
 * @{
 *
 * @review TaylorF2 routines reviewed by Frank Ohme, Andrew Lundgren, Alex Nitz,
 * Alex Nielsen, Salvatore Vitale, Jocelyn Read, Sebastian Khan.
 * The review concluded with git hash 6106138b2140ffb11bc38fc914e0a1de7082dc4d (Nov 2014)
 * Additional tidal terms up to 7.5PN order reviewed by Ohme, Haney, Khan, Samajdar,
 * Riemenschneider, Setyawati, Hinderer. Concluded with git hash
 * f15615215a7e70488d32137a827d63192cbe3ef6 (February 2019).
 *
 * @note If not specified explicitly by the user, the default tidal order will be
 * chosen as 7.0PN, based on the good performance found in
 * https://arxiv.org/pdf/1804.02235.pdf (Fig. 10). However, the user is free to specify
 * any tidal order up to 7.5PN passing the relevant flag through the LALDict.
 *
 * @name Routines for TaylorF2 Waveforms
 * @sa
 * Section IIIF of Alessandra Buonanno, Bala R Iyer, Evan
 * Ochsner, Yi Pan, and B S Sathyaprakash, "Comparison of post-Newtonian
 * templates for compact binary inspiral signals in gravitational-wave
 * detectors", Phys. Rev. D 80, 084043 (2009), arXiv:0907.0700v1
 *
 * @{
 */

/** \brief Returns structure containing TaylorF2 phasing coefficients for given
 *  physical parameters.
 */

//  #define PN_PHASING_SERIES_MAX_ORDER 15
//  typedef struct
//  {
//      REAL8 v[PN_PHASING_SERIES_MAX_ORDER+1];
//      REAL8 vlogv[PN_PHASING_SERIES_MAX_ORDER+1];
//      REAL8 vlogvsq[PN_PHASING_SERIES_MAX_ORDER+1];
//      REAL8 vneg[PN_PHASING_SERIES_MAX_ORDER+1];
//  }
//  PNPhasingSeries;

 
// int XLALSimInspiralTaylorF2AlignedPhasing(
//         PNPhasingSeries **pn,   /**< phasing coefficients (output) */
//         const REAL8 m1,         /**< mass of body 1 */
//         const REAL8 m2,		/**< mass of body 2 */
//         const REAL8 chi1,	/**< aligned spin parameter of body 1 */
//         const REAL8 chi2,	/**< aligned spin parameter of body 2 */
//         LALDict *p              /**< LAL dictionary containing accessory parameters */
// 	)
// {
//     PNPhasingSeries *pfa;

//     if (!pn) XLAL_ERROR(XLAL_EFAULT);
//     if (*pn) XLAL_ERROR(XLAL_EFAULT);


//     pfa = (PNPhasingSeries *) LALMalloc(sizeof(PNPhasingSeries));

//     XLALSimInspiralPNPhasing_F2(pfa, m1, m2, chi1, chi2, chi1*chi1, chi2*chi2, chi1*chi2, p);

//     *pn = pfa;

//     return XLAL_SUCCESS;
// }

// int XLALSimInspiralTaylorF2AlignedPhasingArray(
//         REAL8Vector **phasingvals, /**< phasing coefficients (output) */
//         REAL8Vector mass1, /**< Masses of heavier bodies */
//         REAL8Vector mass2, /**< Masses of lighter bodies */
//         REAL8Vector chi1, /**< Aligned spin of body 1 */
//         REAL8Vector chi2, /**< Aligned spin of body 2 */
//         REAL8Vector lambda1, /**< Tidal deformation of body 1 */
//         REAL8Vector lambda2, /**< Tidal deformation of body 2 */
//         REAL8Vector dquadmon1, /**< Self-spin deformation of body 1 */
//         REAL8Vector dquadmon2 /**< Self-spin deformation of body 2 */
//         )
// {
//     UINT4 idx, jdx;
//     UINT4 pnmaxnum = PN_PHASING_SERIES_MAX_ORDER + 1;
//     LALDict *a=NULL;
//     a=XLALCreateDict();

//     PNPhasingSeries *curr_phasing=NULL;
//     *phasingvals = XLALCreateREAL8Vector(mass1.length * pnmaxnum * 3); 
//     REAL8Vector* pv = *phasingvals;

//     for (idx=0; idx < mass1.length; idx++)
//     {
//         XLALSimInspiralWaveformParamsInsertdQuadMon1(a, dquadmon1.data[idx]);
//         XLALSimInspiralWaveformParamsInsertdQuadMon2(a, dquadmon2.data[idx]);
//         XLALSimInspiralWaveformParamsInsertTidalLambda1(a, lambda1.data[idx]);
//         XLALSimInspiralWaveformParamsInsertTidalLambda2(a, lambda2.data[idx]);

//         XLALSimInspiralTaylorF2AlignedPhasing
//             (&curr_phasing, mass1.data[idx], mass2.data[idx], chi1.data[idx],
//              chi2.data[idx], a);
//         for (jdx=0; jdx < pnmaxnum; jdx++)
//         {
//             pv->data[jdx*mass1.length + idx] = curr_phasing->v[jdx];
//             pv->data[mass1.length*pnmaxnum + jdx*mass1.length + idx] =
//                 curr_phasing->vlogv[jdx];
//             pv->data[mass1.length*pnmaxnum*2 + idx + jdx*mass1.length] =
//                 curr_phasing->vlogvsq[jdx];
//         }
//         LALFree(curr_phasing);
//         curr_phasing=NULL;
//     }

//     XLALDestroyDict(a);

//     return XLAL_SUCCESS;
// }


int XLALSimInspiralTaylorF2Core(
        COMPLEX16FrequencySeries **htilde_out, /**< FD waveform */
	const REAL8Sequence *freqs,            /**< frequency points at which to evaluate the waveform (Hz) */
        const REAL8 phi_ref,                   /**< reference orbital phase (rad) */
        const REAL8 m1_SI,                     /**< mass of companion 1 (kg) */
        const REAL8 m2_SI,                     /**< mass of companion 2 (kg) */
        const REAL8 f_ref,                     /**< Reference GW frequency (Hz) - if 0 reference point is coalescence */
	const REAL8 shft,		       /**< time shift to be applied to frequency-domain phase (sec)*/
        const REAL8 r,                         /**< distance of source (m) */
        PNPhasingSeries *pfaP /**< Phasing coefficients >**/
        )
{

    if (!htilde_out) XLAL_ERROR(XLAL_EFAULT);
    if (!freqs) XLAL_ERROR(XLAL_EFAULT);
    /* external: SI; internal: solar masses */
    const REAL8 m1 = m1_SI / LAL_MSUN_SI;
    const REAL8 m2 = m2_SI / LAL_MSUN_SI;
    const REAL8 m = m1 + m2;
    const REAL8 m_sec = m * LAL_MTSUN_SI;  /* total mass in seconds */
    const REAL8 eta = m1 * m2 / (m * m);
    const REAL8 piM = LAL_PI * m_sec;
    REAL8 amp0;
    size_t i;
    COMPLEX16 *data = NULL;
    LIGOTimeGPS tC = {0, 0};
    INT4 iStart = 0;

    COMPLEX16FrequencySeries *htilde = NULL;

    if (*htilde_out) { //case when htilde_out has been allocated in XLALSimInspiralTaylorF2
	    htilde = *htilde_out;
	    iStart = htilde->data->length - freqs->length; //index shift to fill pre-allocated data
	    if(iStart < 0) XLAL_ERROR(XLAL_EFAULT);
    }
    else { //otherwise allocate memory here
	    htilde = XLALCreateCOMPLEX16FrequencySeries("htilde: FD waveform", &tC, freqs->data[0], 0., &lalStrainUnit, freqs->length);
	    if (!htilde) XLAL_ERROR(XLAL_EFUNC);
	    XLALUnitMultiply(&htilde->sampleUnits, &htilde->sampleUnits, &lalSecondUnit);
    }

    PNPhasingSeries pfa = *pfaP;

    REAL8 pfaN = 0.; REAL8 pfa1 = 0.;
    REAL8 pfa2 = 0.; REAL8 pfa3 = 0.; REAL8 pfa4 = 0.;
    REAL8 pfa5 = 0.; REAL8 pfl5 = 0.;
    REAL8 pfa6 = 0.; REAL8 pfl6 = 0.;
    REAL8 pfa7 = 0.; 
    REAL8 pfa8 = 0.; REAL8 pfl8 = 0.;
    REAL8 pfa9 = 0.; REAL8 pfl9 = 0.;

    INT4 phaseO=6;
    switch (phaseO)
    {
        case -1:
        case 9:
            pfa9 = pfa.v[9];
            pfl9 = pfa.vlogv[9];
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
            __attribute__ ((fallthrough));
#endif
        case 8:
            pfa8 = pfa.v[8];
            pfl8 = pfa.vlogv[8];
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
            __attribute__ ((fallthrough));
#endif
        case 7:
            pfa7 = pfa.v[7];
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
            __attribute__ ((fallthrough));
#endif
        case 6:
            pfa6 = pfa.v[6];
            pfl6 = pfa.vlogv[6];
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
            __attribute__ ((fallthrough));
#endif
        case 5:
            pfa5 = pfa.v[5];
            pfl5 = pfa.vlogv[5];
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
            __attribute__ ((fallthrough));
#endif
        case 4:
            pfa4 = pfa.v[4];
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
            __attribute__ ((fallthrough));
#endif
        case 3:
            pfa3 = pfa.v[3];
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
            __attribute__ ((fallthrough));
#endif
        case 2:
            pfa2 = pfa.v[2];
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
            __attribute__ ((fallthrough));
#endif
        case 1:
            pfa1 = pfa.v[1];
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
            __attribute__ ((fallthrough));
#endif
        case 0:
            pfaN = pfa.v[0];
            break;
        default:
            XLAL_ERROR(XLAL_ETYPE, "Invalid phase PN order %d", phaseO);
    }

    /* Validate expansion order arguments.
     * This must be done here instead of in the OpenMP parallel loop
     * because when OpenMP parallelization is turned on, early exits
     * from loops (via return or break statements) are not permitted.
     */

    /* Validate amplitude PN order. */
    INT4 amplitudeO=-1;
    switch (amplitudeO)
    {
        case -1:
        case 7:
        case 6:
        case 5:
        case 4:
        case 3:
        case 2:
        case 0:
            break;
        default:
            XLAL_ERROR(XLAL_ETYPE, "Invalid amplitude PN order %d", amplitudeO);
    }

    /* The flux and energy coefficients below are used to compute SPA amplitude corrections */

    /* flux coefficients */
    const REAL8 FTaN = XLALSimInspiralPNFlux_0PNCoeff(eta);
    const REAL8 FTa2 = XLALSimInspiralPNFlux_2PNCoeff(eta);
    const REAL8 FTa3 = XLALSimInspiralPNFlux_3PNCoeff(eta);
    const REAL8 FTa4 = XLALSimInspiralPNFlux_4PNCoeff(eta);
    const REAL8 FTa5 = XLALSimInspiralPNFlux_5PNCoeff(eta);
    const REAL8 FTl6 = XLALSimInspiralPNFlux_6PNLogCoeff(eta);
    const REAL8 FTa6 = XLALSimInspiralPNFlux_6PNCoeff(eta);
    const REAL8 FTa7 = XLALSimInspiralPNFlux_7PNCoeff(eta);

    /* energy coefficients */
    const REAL8 dETaN = 2. * XLALSimInspiralPNEnergy_0PNCoeff(eta);
    const REAL8 dETa1 = 2. * XLALSimInspiralPNEnergy_2PNCoeff(eta);
    const REAL8 dETa2 = 3. * XLALSimInspiralPNEnergy_4PNCoeff(eta);
    const REAL8 dETa3 = 4. * XLALSimInspiralPNEnergy_6PNCoeff(eta);


    /* Perform some initial checks */
    if (m1_SI <= 0) XLAL_ERROR(XLAL_EDOM);
    if (m2_SI <= 0) XLAL_ERROR(XLAL_EDOM);
    if (f_ref < 0) XLAL_ERROR(XLAL_EDOM);
    if (r <= 0) XLAL_ERROR(XLAL_EDOM);

    /* extrinsic parameters */
    amp0 = -4. * m1 * m2 / r * LAL_MRSUN_SI * LAL_MTSUN_SI * sqrt(LAL_PI/12.L);

    data = htilde->data->data;

    /* Compute the SPA phase at the reference point
     * N.B. f_ref == 0 means we define the reference time/phase at "coalescence"
     * when the frequency approaches infinity. In that case,
     * the integrals Eq. 3.15 of arXiv:0907.0700 vanish when evaluated at
     * f_ref == infinity. If f_ref is finite, we must compute the SPA phase
     * evaluated at f_ref, store it as ref_phasing and subtract it off.
     */
    REAL8 ref_phasing = 0.;
    if( f_ref != 0. ) {
        const REAL8 vref = cbrt(piM*f_ref);
        const REAL8 logvref = log(vref);
        const REAL8 log4vref = log(4*vref);
        const REAL8 v2ref = vref * vref;
        const REAL8 v3ref = vref * v2ref;
        const REAL8 v4ref = vref * v3ref;
        const REAL8 v5ref = vref * v4ref;
        const REAL8 v6ref = vref * v5ref;
        const REAL8 v7ref = vref * v6ref;
        const REAL8 v8ref = vref * v7ref;
        const REAL8 v9ref = vref * v8ref;
        const REAL8 v10ref = vref * v9ref;
        const REAL8 v12ref = v2ref * v10ref;
        const REAL8 v13ref = vref * v12ref;
        const REAL8 v14ref = vref * v13ref;
        const REAL8 v15ref = vref * v14ref;
        
        ref_phasing += (pfa9 + pfl9 * log4vref) * v9ref;
        ref_phasing += (pfa8 + pfl8 * logvref) * v8ref * logvref;
        ref_phasing += pfa7 * v7ref;
        ref_phasing += (pfa6 + pfl6 * logvref) * v6ref;
        ref_phasing += (pfa5 + pfl5 * logvref) * v5ref;
        ref_phasing += pfa4 * v4ref;
        ref_phasing += pfa3 * v3ref;
        ref_phasing += pfa2 * v2ref;
        ref_phasing += pfa1 * vref;
        ref_phasing += pfaN;

        ref_phasing /= v5ref;
    } /* End of if(f_ref != 0) block */
    
    // FILE *ptr = NULL;
    // ptr = fopen("TaylorF2.txt", "w");
    
    #pragma omp parallel for
    for (i = 0; i < freqs->length; i++) {
        const REAL8 f = freqs->data[i];
        const REAL8 v = cbrt(piM*f);
        const REAL8 logv = log(v);
        const REAL8 log4v = log(4*v);
        const REAL8 v2 = v * v;
        const REAL8 v3 = v * v2;
        const REAL8 v4 = v * v3;
        const REAL8 v5 = v * v4;
        const REAL8 v6 = v * v5;
        const REAL8 v7 = v * v6;
        const REAL8 v8 = v * v7;
        const REAL8 v9 = v * v8;
        const REAL8 v10 = v * v9;
        const REAL8 v12 = v2 * v10;
        const REAL8 v13 = v * v12;
        const REAL8 v14 = v * v13;
        const REAL8 v15 = v * v14;
        REAL8 phasing = 0.;
        REAL8 dEnergy = 0.;
        REAL8 flux = 0.;
        REAL8 amp;
        
        phasing += (pfa9 + pfl9 * log4v) * v9;
        phasing += (pfa8 + pfl8 * logv) * v8 * logv;
        phasing += pfa7 * v7;
        phasing += (pfa6 + pfl6 * logv) * v6;
        phasing += (pfa5 + pfl5 * logv) * v5;
        phasing += pfa4 * v4;
        phasing += pfa3 * v3;
        phasing += pfa2 * v2;
        phasing += pfa1 * v;
        phasing += pfaN;
        
        // printing in txt file
        // fprintf(ptr, "fref=%.2f, f=%.2f, v=%f\n", f_ref, f, (float)v);
        printf("0PN=%.3f,1PN=%.3f,1.5PN=%.3f,2PN=%.3f,2.5PN=%.3f,3PN=%.3f\n",pfaN,pfa2,pfa3,pfa4,(pfa5+pfl5*logv),(pfa6+pfl6*logv));
        printf("phasing(f) = %f, phasing[(f_ref) = %f\n", phasing/v5, ref_phasing );

    /* WARNING! Amplitude orders beyond 0 have NOT been reviewed!
     * Use at your own risk. The default is to turn them off.
     * These do not currently include spin corrections.
     * Note that these are not higher PN corrections to the amplitude.
     * They are the corrections to the leading-order amplitude arising
     * from the stationary phase approximation. See for instance
     * Eq 6.9 of arXiv:0810.5336
     */
	switch (amplitudeO)
        {
            case 7:
                flux += FTa7 * v7;
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
                __attribute__ ((fallthrough));
#endif
            case 6:
                flux += (FTa6 + FTl6*logv) * v6;
                dEnergy += dETa3 * v6;
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
                __attribute__ ((fallthrough));
#endif
            case 5:
                flux += FTa5 * v5;
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
                __attribute__ ((fallthrough));
#endif
            case 4:
                flux += FTa4 * v4;
                dEnergy += dETa2 * v4;
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
                __attribute__ ((fallthrough));
#endif
            case 3:
                flux += FTa3 * v3;
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
                __attribute__ ((fallthrough));
#endif
            case 2:
                flux += FTa2 * v2;
                dEnergy += dETa1 * v2;
#if __GNUC__ >= 9 && !defined __INTEL_COMPILER
                __attribute__ ((fallthrough));
#endif
            case -1: /* Default to no SPA amplitude corrections */
            case 0:
                flux += 1.;
                dEnergy += 1.;
        }

        phasing /= v5;
        flux *= FTaN * v10;
        dEnergy *= dETaN * v;
        // Note the factor of 2 b/c phi_ref is orbital phase
        phasing += shft * f - 2.*phi_ref - ref_phasing;


        printf("psi(f) = %f\n", phasing);


        amp = amp0 * sqrt(-dEnergy/flux) * v;
        data[i+iStart] = amp * cos(phasing - LAL_PI_4)
                - amp * sin(phasing - LAL_PI_4) * 1.0j;
        
        REAL8 cfac = cos(0.4);
        REAL8 pfac = 0.5 * (1. + cfac*cfac);
        COMPLEX16 test = cos(phasing-0.7853981633974483) - I*sin(phasing-0.7853981633974483);
        printf("hf0_p = = %.5e+i*%.5e\n", creal(test),cimag(test));
        printf("f = %d, x = %.3e, Amp = %.5e, hp = %.3e+i*%.3e , hx = %.5e+i*%.5e\n\n", (int)f,(float)v*v, (float)amp, creal(data[i+iStart]*pfac),cimag(data[i+iStart]*pfac), creal(data[i+iStart]*-I*cfac),cimag(data[i+iStart]*-I*cfac));
        // amp = AmpEcck*-4 
    }
    
    // fclose(ptr);

    *htilde_out = htilde;
    return XLAL_SUCCESS;
}

/**
 * Computes the stationary phase approximation to the Fourier transform of
 * a chirp waveform. The amplitude is given by expanding \f$1/\sqrt{\dot{F}}\f$.
 * If the PN order is set to -1, then the highest implemented order is used.
 *
 * @note f_ref is the GW frequency at which phi_ref is defined. The most common
 * choice in the literature is to choose the reference point as "coalescence",
 * when the frequency becomes infinite. This is the behavior of the code when
 * f_ref==0. If f_ref > 0, phi_ref sets the orbital phase at that GW frequency.
 *
 * See arXiv:0810.5336 and arXiv:astro-ph/0504538 for spin corrections
 * to the phasing.
 * See arXiv:1303.7412 for spin-orbit phasing corrections at 3 and 3.5PN order
 *
 * The spin and tidal order enums are defined in LALSimInspiralWaveformFlags.h
 */
int XLALSimInspiralTaylorF2(
        COMPLEX16FrequencySeries *htilde_out, /**< FD waveform */
        const REAL8 phi_ref,                   /**< reference orbital phase (rad) */
        const REAL8 deltaF,                    /**< frequency resolution */
        const REAL8 m1_SI,                     /**< mass of companion 1 (kg) */
        const REAL8 m2_SI,                     /**< mass of companion 2 (kg) */
        const REAL8 S1z,                       /**<  z component of the spin of companion 1 */
        const REAL8 S2z,                       /**<  z component of the spin of companion 2  */
        const REAL8 fStart,                    /**< start GW frequency (Hz) */
        const REAL8 fEnd,                      /**< highest GW frequency (Hz) of waveform generation - if 0, end at Schwarzschild ISCO */
        const REAL8 f_ref,                     /**< Reference GW frequency (Hz) - if 0 reference point is coalescence */
        const REAL8 r                         /**< distance of source (m) */
        )
{
    /* external: SI; internal: solar masses */
    const REAL8 m1 = m1_SI / LAL_MSUN_SI;
    const REAL8 m2 = m2_SI / LAL_MSUN_SI;
    const REAL8 m = m1 + m2;
    const REAL8 m_sec = m * LAL_MTSUN_SI;  /* total mass in seconds */
    // const REAL8 eta = m1 * m2 / (m * m);
    const REAL8 piM = LAL_PI * m_sec;
    const REAL8 vISCO = 1. / sqrt(6.);
    const REAL8 fISCO = vISCO * vISCO * vISCO / piM;
    //const REAL8 m1OverM = m1 / m;
    // const REAL8 m2OverM = m2 / m;
    REAL8 shft, f_max;
    size_t i, n;
    INT4 iStart;
    REAL8Sequence *freqs = NULL;
    LIGOTimeGPS tC = {0, 0};
    int ret;
    int retcode;
    REAL8 fCONT;
    // INT4 tideO = XLALSimInspiralWaveformParamsLookupPNTidalOrder(p);
    // REAL8 lambda1 = XLALSimInspiralWaveformParamsLookupTidalLambda1(p);
    // REAL8 lambda2 = XLALSimInspiralWaveformParamsLookupTidalLambda2(p);
    // retcode = XLALSimInspiralSetQuadMonParamsFromLambdas(p);
    // XLAL_CHECK(retcode == XLAL_SUCCESS, XLAL_EFUNC, "Failed to set quadparams from Universal relation.\n");

    COMPLEX16FrequencySeries *htilde = NULL;

    /* Perform some initial checks */
    // if (!htilde_out) XLAL_ERROR(XLAL_EFAULT);
    // if (*htilde_out) XLAL_ERROR(XLAL_EFAULT);
    // if (m1_SI <= 0) XLAL_ERROR(XLAL_EDOM);
    // if (m2_SI <= 0) XLAL_ERROR(XLAL_EDOM);
    // if (fStart <= 0) XLAL_ERROR(XLAL_EDOM);
    // if (f_ref < 0) XLAL_ERROR(XLAL_EDOM);
    // if (r <= 0) XLAL_ERROR(XLAL_EDOM);

    // /* allocate htilde */
    // if (( fEnd == 0. ) && ( tideO == 0 )) // End at ISCO
    //     f_max = fISCO;
    // else if (( fEnd == 0. ) && ( tideO != 0 )) { // End at the minimum of the contact and ISCO frequencies only when tides are enabled
    //     fCONT = XLALSimInspiralContactFrequency(m1, lambda1, m2, lambda2); /* Contact frequency of two compact objects */
    //     f_max = (fCONT > fISCO) ? fISCO : fCONT;
    // }
    // else // End at user-specified freq.
    //     f_max = fEnd;
    // if (f_max <= fStart) XLAL_ERROR(XLAL_EDOM);

    // setting the maximum frequency 
    if ( fEnd == 0. ) // End at ISCO
        f_max = 1024.;
    else // End at user-specified freq.
        f_max = fEnd;
    if (f_max <= fStart) XLAL_ERROR(XLAL_EDOM);

    n = (size_t) (f_max / deltaF + 1);
    XLALGPSAdd(&tC, -1 / deltaF);  /* coalesce at t=0 */
    htilde = XLALCreateCOMPLEX16FrequencySeries("htilde: FD waveform", &tC, 0.0, deltaF, &lalStrainUnit, n);
    if (!htilde) XLAL_ERROR(XLAL_EFUNC);
    memset(htilde->data->data, 0, n * sizeof(COMPLEX16));
    XLALUnitMultiply(&htilde->sampleUnits, &htilde->sampleUnits, &lalSecondUnit);

    /* Fill with non-zero vals from fStart to f_max */
    iStart = (INT4) ceil(fStart / deltaF);

    /* Sequence of frequencies where waveform model is to be evaluated */
    freqs = XLALCreateREAL8Sequence(n - iStart);

    /* extrinsic parameters */
    shft = LAL_TWOPI * (tC.gpsSeconds + 1e-9 * tC.gpsNanoSeconds);
    
    // writing result components in txt file

    #pragma omp parallel for
    for (i = iStart; i < n; i++) {
        freqs->data[i-iStart] = i * deltaF;
    }

    /* phasing coefficients */
    PNPhasingSeries pfa;
    XLALSimInspiralPNPhasing_F2(&pfa, m1, m2, S1z, S2z, S1z*S1z, S2z*S2z, S1z*S2z);

    ret = XLALSimInspiralTaylorF2Core(&htilde, freqs, phi_ref, m1_SI, m2_SI,
                                      f_ref, shft, r, &pfa);

    XLALDestroyREAL8Sequence(freqs);

    // *htilde_out = htilde;

    return ret;
}

/** @} */
/** @} */
int main()
{
    const REAL8 phiRef = 1.2;                   /**< reference orbital phase (rad) */
    const REAL8 deltaF = 10.0;                    /**< frequency resolution */
    const REAL8 m1 = 6.5*LAL_MSUN_SI;                     /**< mass of companion 1 (kg) */
    const REAL8 m2 = 5.8*LAL_MSUN_SI;                     /**< mass of companion 2 (kg) */
    const REAL8 S1z=0.;                       /**<  z component of the spin of companion 1 */
    const REAL8 S2z=0.;                       /**<  z component of the spin of companion 2  */
    const REAL8 f_min=10.;                    /**< start GW frequency (Hz) */
    const REAL8 f_max=30.;                      /**< highest GW frequency (Hz) of waveform generation - if 0, end at Schwarzschild ISCO */
    const REAL8 f_ref=10.;                     /**< Reference GW frequency (Hz) - if 0 reference point is coalescence */
    const REAL8 distance = 200*LAL_PC_SI*1e6;                         /**< distance of source (m) */
    
    COMPLEX16FrequencySeries *hptilde;
    COMPLEX16FrequencySeries *hctilde;
    
    LIGOTimeGPS tC = {0, 0};
    size_t n;
    n = (size_t) (f_max / deltaF + 1);
    XLALGPSAdd(&tC, -1 / deltaF);
    
    hptilde = XLALCreateCOMPLEX16FrequencySeries("htilde_p: FD waveform", &tC, f_min, deltaF, &lalStrainUnit, n);
    memset(hptilde->data->data, 0, n * sizeof(COMPLEX16));
    
    hctilde = XLALCreateCOMPLEX16FrequencySeries("htilde_c: FD waveform", &tC, f_min, deltaF, &lalStrainUnit, n);
    memset(hptilde->data->data, 0, n * sizeof(COMPLEX16));
    
    /* Call the waveform driver routine */
    int ret;
    ret = XLALSimInspiralTaylorF2(hptilde, phiRef, deltaF, m1, m2, S1z, S2z, f_min, f_max, f_ref, distance);
    
    return 0;
}
// gcc test.c -lm -ldl -l lal
// ./a.out

// 0PN=0.094,1PN=0.605,1.5PN=-4.728,2PN=4.344,2.5PN=-76.712,3PN=-39.554
// phasing(f) = 3181.348868, phasing[(f_ref) = 3181.348868
// psi(f) = -8.683185
// hf0_p = = -9.99041e-01+i*-4.37915e-02
// f = 10, x = 1.536e-02, Amp = -1.075e-22, hp = 1.074e-22+i*4.706e-24 

// 0PN=0.094,1PN=0.605,1.5PN=-4.728,2PN=4.344,2.5PN=-66.612,3PN=-46.640
// phasing(f) = 932.547006, phasing[(f_ref) = 3181.348868
// psi(f) = -2263.768232
// hf0_p = = -8.60436e-01+i*5.09559e-01
// f = 20, x = 2.438e-02, Amp = -4.787e-23, hp = 4.119e-23+i*-2.439e-23 

// 0PN=0.094,1PN=0.605,1.5PN=-4.728,2PN=4.344,2.5PN=-60.703,3PN=-50.786
// phasing(f) = 428.192163, phasing[(f_ref) = 3181.348868
// psi(f) = -2774.406261
// hf0_p = = -3.94433e-01+i*-9.18925e-01
// f = 30, x = 3.195e-02, Amp = -2.983e-23, hp = 1.177e-23+i*2.741e-23 